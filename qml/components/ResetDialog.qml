import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Dialog {
    id: resetDialog

    title: i18n.tr("Reset Puzzle")
    text: i18n.tr("Choose one of the levels to reset the puzzle to. This can't be undone")

    ComboButton {
        id: levelChosen
        text: i18n.tr("Choose a level")
        expandedHeight: units.gu(30)

        onClicked: expanded = false

        UbuntuListView {
            width: parent.width
            height: levelChosen.comboListHeight
            model: ["easy", "medium","hard"]

            delegate: ListItem {
                ListItemLayout {
                    title.text: modelData
                }

                onClicked: {
                    levelChosen.expanded = false
                    levelChosen.text = modelData
                }
            }
        }
    }

    Button {
        id: mainAction
        text: i18n.tr("Reset")
        color: theme.palette.normal.negative
        enabled: levelChosen.text !== i18n.tr("Choose a level")

        onClicked: {
            root.currentLevel = levelChosen.text
            leftTop.circleColor  = pieceForLevel(levelChosen.text,"left")
            rightTop.circleColor = pieceForLevel(levelChosen.text,"right")

            PopupUtils.close(resetDialog)
        }
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: PopupUtils.close(resetDialog)
    }
}

