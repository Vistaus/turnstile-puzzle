/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Turnstile Puzzle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'turnstile.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    backgroundColor: "#333"

    property bool isLandscape: width > height
    property string defaultLevel: "easy"
    property string currentLevel: "easy"
    readonly property string mainColor: "#f7f7f7"

    readonly property var levelEasy: {
        "left": {
            "center": "strawberry",
            "pieces" : "strawberry,strawberry,strawberry,blueish,blueish,blueish,strawberry,strawberry,strawberry,strawberry,strawberry,strawberry"
        },
        "right": {
            "center": "blueish",
            "pieces": "blueish,blueish,blueish,blueish,blueish,blueish,blueish,blueish,blueish,blueish,blueish,blueish"
        }
    }

    readonly property var levelMedium: {
        "left": {
            "center": "silk",
            "pieces" : "green,green,green,green,silk,ochre,ochre,ochre,ochre,ochre,silk,green"
        },
        "right": {
            "center": "silk",
            "pieces": "green,green,green,green,silk,ochre,ochre,ochre,ochre,ochre,silk,green"
        }
    }

    readonly property var levelHard: {
        "left": {
            "center": "ash",
            "pieces" : "blueish,blueish,ash,ochre,ochre,ochre,ash,green,green,green,ash,blueish"
        },
        "right": {
            "center": "ash",
            "pieces": "ash,strawberry,strawberry,strawberry,ash,yellow,yellow,yellow,ash,ochre,ochre,ochre"
        }
    }

    PageStack {
        id: mainPageStack
        width: parent.width
        height: parent.height

        Component.onCompleted: mainPageStack.push(mainPageComponent);
    }

    Component {
        id: mainPageComponent

        Page {
            id: mainPage
            width: parent.width
            height: parent.height

            property int maxSize: Math.max(width, height - header-height)
            signal checkIfCompleted

            onCheckIfCompleted: {
                if (mainPage.finished()) {
                    PopupUtils.open(successDialog)
                }
            }

            header: PageHeader {
                id: header
                title: i18n.tr('Turnstile Puzzle')

                trailingActionBar {
                    actions: [
                        Action {
                            id: actionInfo
                            iconName: "info"
                            shortcut: "Ctrl+i"
                            text: i18n.tr("Information")
                            onTriggered: {
                                Qt.inputMethod.hide();
                                mainPageStack.push(Qt.resolvedUrl("About.qml"));
                            }
                        },

                        Action {
                            id: actionSettings
                            iconName: "media-playlist-shuffle"
                            shortcut: "Ctrl+s"
                            text: i18n.tr("Shuffle")
                            onTriggered: PopupUtils.open(shuffleDialog)
                        },

                        Action {
                            iconName: "reset"
                            text: i18n.tr("Reset")
                            onTriggered: PopupUtils.open(resetDialog)
                        }
                    ]
                }
            }

            Item {
                id: board
                rotation: isLandscape ? 0 : -90
                width: maxSize <= units.gu(70)
                        ? units.gu(58)
                        : maxSize <= units.gu(145)
                            ? units.gu(118)
                            : units.gu(160)

                height: relativeCircleSize

                anchors {
                    centerIn: parent
                    verticalCenterOffset: header.height / 2
                }

                property real relativeCircleSize: (board.width / 2) * 1.24

                Circle {
                    id: leftTop
                    circleSize: board.relativeCircleSize

                    circleColor: pieceForLevel(defaultLevel,"left")
                    visualCircleColor: pieceForLevel(defaultLevel,"left")

                    anchors {
                        top: parent.top
                        left: parent.left
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        rightTop.showSelection = false;
                    }

                    onColorsUpdated: {
                        rightTop.circleColor.colorOrdered[11] = leftTop.circleColor.colorOrdered[3]
                        rightTop.circleColor.colorOrdered[10] = leftTop.circleColor.colorOrdered[4]
                        rightTop.circleColor.colorOrdered[9] = leftTop.circleColor.colorOrdered[5]
                        //Send changed signals for the pieces to update
                        rightTop.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 1
                }

                Circle {
                    id: rightTop
                    circleSize: board.relativeCircleSize

                    circleColor: pieceForLevel(defaultLevel,"right")
                    visualCircleColor: pieceForLevel(defaultLevel,"right")

                    anchors {
                        top: parent.top
                        right: parent.right
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        leftTop.showSelection = false;
                    }

                    onColorsUpdated: {
                        leftTop.circleColor.colorOrdered[3] = rightTop.circleColor.colorOrdered[11]
                        leftTop.circleColor.colorOrdered[4] = rightTop.circleColor.colorOrdered[10]
                        leftTop.circleColor.colorOrdered[5] = rightTop.circleColor.colorOrdered[9]
                        //Send changed signals for the pieces to update
                        leftTop.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 2
                }

            }

            Component {
                id: successDialog

                SuccessDialog {}
            }

            Component {
                id: resetDialog

                ResetDialog {}
            }

            Component {
                id: shuffleDialog

                ShuffleDialog {}
            }

            function shuffle() {
                var leftPieces = pieceForLevel(currentLevel,"left")
                var rightPieces = pieceForLevel(currentLevel,"right")
                //Delete the doubled colors
                rightPieces.colorOrdered.splice(9,11)

                var oddPieces = []
                var evenPieces = []

                //We need to randomize odd and even pieces separately to keep the pattern
                for (var k=0; k < leftPieces.colorOrdered.length; k++) {
                    if (k % 2 == 0 ) {
                        evenPieces.push(leftPieces.colorOrdered[k])
                    } else {
                        oddPieces.push(leftPieces.colorOrdered[k])
                    }
                }

                for (var l=0; l < rightPieces.colorOrdered.length; l++) {
                    if (l % 2 == 0 ) {
                        evenPieces.push(rightPieces.colorOrdered[l])
                    } else {
                        oddPieces.push(rightPieces.colorOrdered[l])
                    }
                }

                var randomOddPieces = []
                var randomEvenPieces = []

                var oddPiecesLength =  oddPieces.length
                var evenPiecesLength =  evenPieces.length

                for (var j = 0; j < oddPiecesLength; j++) {
                    var randomElement = Math.floor(Math.random() * oddPieces.length)
                    randomOddPieces.push(oddPieces.splice(randomElement,1))
                }

                for (var j = 0; j < evenPiecesLength; j++) {
                    var randomElement = Math.floor(Math.random() * evenPieces.length)
                    randomEvenPieces.push(evenPieces.splice(randomElement,1))
                }

                for (var k=0; k<12; k++) {
                    if (k % 2 == 0 ) {
                        leftTop.circleColor.colorOrdered[k] = randomEvenPieces.pop()
                    } else {
                        leftTop.circleColor.colorOrdered[k] = randomOddPieces.pop()
                    }
                }

                rightTop.circleColor.colorOrdered[9] = leftTop.circleColor.colorOrdered[5]
                rightTop.circleColor.colorOrdered[10] = leftTop.circleColor.colorOrdered[4]
                rightTop.circleColor.colorOrdered[11] = leftTop.circleColor.colorOrdered[3]

                for (var k=0; k<9; k++) {
                    if (k % 2 == 0 ) {
                        rightTop.circleColor.colorOrdered[k] = randomEvenPieces.pop()
                    } else {
                        rightTop.circleColor.colorOrdered[k] = randomOddPieces.pop()
                    }
                }

                rightTop.circleColorChanged()
                leftTop.circleColorChanged()
            }

            function finished() {
                var left = leftTop.circleColor.colorOrdered
                var right = rightTop.circleColor.colorOrdered
                var leftFinshed = pieceForLevel(currentLevel,"left").colorOrdered
                var rightFinished = pieceForLevel(currentLevel,"right").colorOrdered

                return left.join() == leftFinshed.join() && right.join() == rightFinished.join()
            }

            function pieceForLevel(level, piece) {
                switch (level) {
                    case "easy":
                        return {
                            "center": root.levelEasy[piece].center,
                            "colorOrdered": root.levelEasy[piece].pieces.split(",")
                        }
                        break
                    case "medium":
                        return {
                            "center": root.levelMedium[piece].center,
                            "colorOrdered": root.levelMedium[piece].pieces.split(",")
                        }
                        break
                    case "hard":
                        return {
                            "center": root.levelHard[piece].center,
                            "colorOrdered": root.levelHard[piece].pieces.split(",")
                        }
                        break
                }
            }
        }
    }
}
